# Schulkonnektor-Lehmannzentrum

Konzept für Schulkonnektor, offen für alle in TUD/BING

* git-st main page https://git-st.inf.tu-dresden.de/stgroup/stwiki/-/wikis/Projects/SchulkonnektorLehmannzentrum/schulkonnektor-main
* gitlab-chemnitz wiki page 
* gitlab-chemnitz repo page https://gitlab.hrz.tu-chemnitz.de/ua1--tu-dresden.de/schulkonnektor-lehmannzentrum
* codiMD master page https://md.inf.tu-dresden.de/schulkonnektor-lehmannzentrum
